package com.mek.app1

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ButtonHello: Button = findViewById(R.id.buttonHello)
        val NameTextView: TextView = findViewById(R.id.name)
        val IdTextView: TextView = findViewById(R.id.id)


        ButtonHello.setOnClickListener {

            Log.d(TAG, " " +NameTextView.text)
            Log.d(TAG, " " +IdTextView.text)
            val message = NameTextView.text
            val intent = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE,message)
            }
            startActivity(intent)
        }
    }
}